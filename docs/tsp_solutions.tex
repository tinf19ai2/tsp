\chapter{Lösungsansätze für das Traveling Salesman Problem}\label{ch:lösungsansätze-für-das-traveling-salesman-problem}
Zur Lösung des Traveling Salesman Problems wurden in der Vergangenheit viele Ansätze geliefert.
Unterteilt werden die Ansätze in exakte Lösungsverfahren und approximierende Verfahren.
Exakte Lösungsverfahren können garantiert die optimale Tour ermitteln, bestehen aber aus deutlich aufwendigeren Berechnungen als die approximierenden Verfahren.
Diese verwenden Heuristiken mit denen sie eine oberere Schranke für das Problem berechnen können, die oft sehr nah an der optimalen Lösung sind.
Im Folgenden wird jeweils ein Ansatz vorgestellt und an einem Beispiel erläutert.
\section{Branch and Bound - Ein Ansatz zur exakten Bestimmung der optimalen Lösung} \label{sec:branchandboundalgosection}
Der Lösungsansatz Branch and Bound (\textit{BaB}) ist einer der effektivsten Ansätze zum Lösen von ganzzahligen Problemen.\cite{williams2013ModelBuildingMP}
Der Algorithmus ermöglicht es uns eine optimale Lösung zu erhalten, ohne die Kosten aller Möglichkeiten zu berechnen. \\
Der Begriff Branch steht für die Verzweigung, die wir uns wie an einem Baum vorstellen können.
Jeder Zweig stellt dann ein Teilproblem dar, in die der Aufteilungsschritt das Gesamtproblem teilt.
Für die Aufteilung werden dann verschiedene Such-Algorithmen wie die Breiten- oder die Tiefensuche verwendet.
Im Nachfolgenden Beispiel verwenden wir die Breitensuche.\\
Der Begriff Bound steht für Schranken.
In diesem Kontext handelt es sich um zwei Schranken, die die Kosten der optimalen Tour nach oben und unten eingrenzen.
Touren deren Kosten höher sind als die obere Schranke werden direkt gestrichen und kein weiterer nachfolgender Knoten des Pfads wird berechnet.
Dadurch entfallen unnötige Rechenschritte und der Lösungsansatz wird schneller.
Die obere Schranke wird bei jeder gefundenen Tour ausgetauscht, deren Gesamtkosten niedriger sind als die aktuelle obere Schranke.
Für die untere Schranke wird in vielen ganzzahligen Problemen eine LP-Relaxation angewandt, bei der die Bedingung der Ganzzahligkeit entfällt.
In unserem Beispiel berechnen wir die Kosten des ersten Knotens und nutzen das als untere Schranke.\cite{MataiSinghMittal2010TSPOverview}\\
Zur Veranschaulichung folgt die Durchführung des Branch and Bound Algorithmus auf ein asymmetrisches Taveling Salesman Problem\footnote{Die Distanz von Stadt $i$ zur Stadt $j$ ist nicht gleich groß wie die Distanz der umgekehrten Strecke.} mit vier Städten $1,2,3,4$.
Das Problem wird hier als gewichteter Graph dargestellt.
\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \node[circle,draw] (1) {$1$};
        \node[circle,draw, right=3cm of 1] (2) {$2$};
        \node[circle,draw, below=3cm of 1] (3) {$3$};
        \node[circle,draw, right=3cm of 3] (4) {$4$};
        \draw[<->,thick] (1) -- (2);
        \draw[<->,thick] (1) -- (3);
        \draw[<->,thick] (1) -- (4);
        \draw[<->,thick] (2) -- (3);
        \draw[<->,thick] (2) -- (4);
        \draw[<->,thick] (3) -- (4);
    \end{tikzpicture}
    \caption{Veranschaulichung des Beispiels zur Erläuterung des Branch and Bound Algorithmus}
    \label{fig:branchAndBoundExample}
\end{figure}
Die Distanz der Strecken zwischen den Städten $1,2,3,4$ sind in folgender Matrix dargestellt:
\begin{equation} \label{eq:babmatrixone}
\begin{bmatrix}
    \infty & 16 & 7 & 16 \\
    20 & \infty & 10 & 30 \\
    2 & 16 & \infty & 15 \\
    6 & 18 & 3 & \infty
\end{bmatrix}
\end{equation}
Durch Reduktion der Matrix\footnote{Eine Matrix $A$ ist reduziert, wenn das Minimum jeder Spalte und jeder Zeile $0$ beträgt: $a_{ij}\in A \, | \, \exists i: a_{ij}=0 j \in {1,2,3,4}\, \land \, \exists j: a_{ij}=0 i \ in {1,2,3,4}$}~\ref{eq:babmatrixone} erhalten wir die Matrix für den Knoten 1.
Dabei entstehen die Kosten $c_1=43$ für die aktuell Tour $(1)$.
Die Kosten setzen sich dazu aus den Reduktionsschritten der Matrix zusammen.
\\[0.5cm]
\textbf{Einschub 1 (Kosten einer Reduktion):}\\
Für die Reduktion werden im ersten Schritt das Minimum für jede Zeile gesucht.
In unserem Beispiel in Matrix~\ref{eq:babmatrixone} sind das von oben nach unten $7,10,2,3$.
Von jedem Element der Zeilen wird das Zeilenminimum subtrahiert.
Im nächsten Schritt wird das gleiche für alle Spalten der aus dem ersten Schritt entstandenen Matrix durchgeführt.
Dabei erhalten wir die Minima von $3,9,0,9$.
Zum Berechnen der Kosten werden jetzt alle Minima von Zeilen und Spalten aufsummiert.
\\[0.2cm]
\hspace*{1.3cm}
$c_1=7+10+2+3+3+9+0+9=43$ \\[0.5cm]
Gleichzeitig setzen wir diese Kosten als untere Schranke unseres Problems.
Als obere Schranke setzen wir $\infty$ bis wir die erste vollständige Tour gefunden haben.
\begin{equation} \label{eq:babmatrixtwo}
\begin{bmatrix}
    \infty & 0 & 0 & 0 \\
    7 & \infty & 0 & 11 \\
    0 & 5 & \infty & 4 \\
    0 & 6 & 0 & \infty
\end{bmatrix}
\end{equation}
Im nächsten Schritt werden ausgehend von Knoten 1 alle von dort aus erreichbaren Knoten berechnet.
Für die Strecke ausgehend von Knoten 1 zum Knoten 2 $(1\rightarrow2)$ wird die erste Zeile, die zweite Spalte und die Kosten für die gewählte Strecke $(a_{12})$ der Matrix aus Gleichung~\ref{eq:babmatrixtwo} durch $\infty$ ersetzt.
Wir erhalten für den Knoten 2 die bereits reduzierte Matrix:
\begin{equation} \label{eq:babmatrixthree}
\begin{bmatrix}
    \infty & \infty & \infty & \infty \\
    \infty & \infty & 0 & 11 \\
    0 & \infty & \infty & 4 \\
    0 & \infty & 0 & \infty
\end{bmatrix}
\end{equation}
Die Kosten für die Matrix~\ref{eq:babmatrixthree} berechnen sich mit
\\[0.2cm]
\hspace*{1.3cm}
$c_2=c(1,2) + \gamma + \hat{\gamma}$,
\\[0.2cm]
wobei $\gamma$ die Kosten der Reduktion des ausgehenden Knotens ist und $\hat{\gamma}$ die Kosten der Reduktion des ankommenden Knotens.
Damit errechnen sich für den zweiten Knoten die Kosten $c_2=50$ $(1\rightarrow2)$.\\
Diese Rechnung führen wir jetzt auch für die Knoten 3 und 4 durch.
Dann erhalten wir folgenden Baum mit zwei Ebenen:
\begin{figure}[H]
    \centering
    \Tree [.1$^{(c=43)}$ 2$^{(c=50)}$ 3$^{(c=54)}$ 4$^{(c=48)}$ ]\label{fig:babtreeone}
\end{figure}
Im nächsten Schritt suchen wir auf der zweiten Ebene des Baums nach dem Knoten mit den minimalen Kosten.
In diesem Fall hat der Knoten 4 die geringsten Kosten.
Für diesen Knoten erweitern wir den Baum jetzt um die beiden Knoten 5 (Stadt 2) und 6 (Stadt 3), die von 4 aus noch begangen werden müssen.
Mit der Matrix für den Knoten 4
\begin{equation} \label{eq:babmatrixthree2}
\begin{bmatrix}
    \infty & \infty & \infty & \infty \\
    7 & \infty & 0 & \infty \\
    0 & 0 & \infty & \infty \\
    \infty & 1 & 0 & \infty
\end{bmatrix}
\end{equation}
berechnen wir dann genauso wie für die Strecke $(1\rightarrow2)$ die Matrixen für die Knoten 5 $(1\rightarrow4\rightarrow2)$ und 6 $(1\rightarrow4\rightarrow3)$.
Wir erhalten die Matrizen
\begin{equation*} \label{babMatrixThree}
\begin{array}{c@{\hspace{8em}}c}
    \text{Knoten 5} & \text{Knoten 6} \\[\normalbaselineskip]
    \begin{bmatrix}
        \infty & \infty & \infty & \infty \\
        7 & \infty & 0 & \infty \\
        0 & \infty & \infty & \infty \\
        \infty & \infty & \infty & \infty
    \end{bmatrix} &
    \begin{bmatrix}
        \infty & \infty & \infty & \infty \\
        0 & \infty & \infty & \infty \\
        0 & 0 & \infty & \infty \\
        \infty & \infty & \infty & \infty
    \end{bmatrix}
\end{array}
\end{equation*}
und den neuen Graph:
\begin{figure}[H]
    \centering
    \Tree [.1$^{(c=43)}$ 2$^{(c=50)}$ 3$^{(c=54)}$ [ .4$^{(c=48)}$ 5$^{(c=49)}_2$ 6$^{(c=55)}_3$ ] ]\label{fig:babtreetwo}
\end{figure}
Für den nächsten Schritt wählen wir den Knoten 5, weil dieser die geringsten Kosten hat.
Für den Knoten 5 berechnen wir den Knoten 7 $(1\rightarrow4\rightarrow2\rightarrow3)$, das Blatt des Zweigs und damit den Abschluss der Tour.
Wenden wir für den Knoten 7 auch die Reduzierungsschritte an, erhalten wir die Kosten $c_7=56$ $(1\rightarrow4\rightarrow2\rightarrow3)$.
Da wir jetzt erstmals eine gesamte Tour gefunden haben, setzen wir diese Kosten als obere Grenze ($upper=56$).\\
Jetzt schauen wir uns wieder den gesamten Graphen an.
Darin suchen wir die Knoten, die keinen Nachfolgeknoten haben und deren Kosten geringer sind, als die obere Schranke. 
Davon wählen wir den Knoten mit den kleinsten Kosten.
Wir wählen den Knoten 2 und berechnen für den weiteren Verlauf des Zweigs die Kosten.
Wir erhalten folgenden Graphen:
\begin{figure}[H]
    \centering
    \Tree [.1$^{(c=43)}$ [ .2$^{(c=50)}$ 8$^{(c=54)}_3$ 9$^{(c=61)}_4$ ] 3$^{(c=54)}$ [ .4$^{(c=48)}$ [ .5$^{(c=49)}_2$ 7$^{(c=56)}_3$ ] 6$^{(c=55)}_3$ ] ]\label{fig:babtreethree}
\end{figure}
Für den Knoten 8 $(1\rightarrow2\rightarrow3)$ berechnen wir auch hier den letzten Knoten 10 $(1\rightarrow2\rightarrow3\rightarrow4)$, der Kosten von $c_10=54$ hat.
Wir vergleichen das mit der aktuellen oberen Grenze. Da der Wert geringer ist ersetzen wir die obere Grenze ($upper=54$) und streichen alle offenen Knoten raus, die höhere Kosten haben.
Damit eliminieren wir die Knoten 6 und 9. \\
Da der Knoten 3 die gleichen Kosten wie der Knoten 10 hat, berechnen wir auch für diesen Knoten die beiden nachfolgenden Knoten.
Wir erhalten den Baum:
\begin{figure}[H]
    \centering
    \Tree [.1$^{(c=43)}$ [ .2$^{(c=50)}$ [ .8$^{(c=54)}_3$ 10$^{(c=54)}_4$ ] \sout{9}$^{(c=61)}_4$ ] [ .3$^{(c=54)}$ \sout{11}$^{(c=58)}_2$ \sout{12}$^{(c=59)}_3$ ] [ .4$^{(c=48)}$ [ .5$^{(c=49)}_2$ \sout{7}$^{(c=56)}_3$ ] \sout{6}$^{(c=55)}_3$ ] ]\label{fig:babtreefour}
\end{figure}
Die Kosten der neu errechneten Knoten sind höher als die obere Grenze und entfallen damit direkt.\\
Damit haben wir für das Problem aus Figur~\ref{fig:branchAndBoundExample} und der Matrix~\ref{eq:babmatrixone} die optimale Tour gefunden.
Da der Handlungsreisende nach dem Besuchen aller Städte zurück zum Ausgangspunkt reisen soll, verläuft die Tour folgendermaßen:
\\[0.2cm]
\hspace*{1.3cm}
$1\rightarrow2\rightarrow3\rightarrow4\rightarrow1$
\section{Nearest-Neighbor-Heuristik - Ein Ansatz zum Finden einer approximierten Lösung}\label{subsec:nearest-neighbor-heuristik---ein-ansatz-zur-finden-einer-approximierten-lösung}
Die Nearest-Neighbor-Heuristik (\textit{NN}) ist ein algorithmischer Lösungsansatz für Optimierungsprobleme wie das Traveling Salesman Problem.
Mit der Heuristik kann nicht garantiert werden, dass eine optimale Tour gefunden wird.
In der Optimierung wird sie aber oft für große Probleme verwendet, um eine Lösung zu finden, die fast optimal ist.
Diese Lösung kann dann mit anderen Algorithmen wie beispielsweise dem Branch and Bound Algorithmus (s. Kapitel~\ref{sec:branchandboundalgosection}) optimiert werden.
Dann wird die Gesamtstrecke der von der NN-Heuristik berechneten Tour als obere Schranke verwendet.
Dadurch entfallen schon viele Zweige im Baum des BaB-Algorithmus und dieser kann effizienter gelöst werden.\cite{evinNuriyeva2013NN}\\
Für ein Traveling Salesman Problem mit $n$ Städten, die als Knoten $v_i \; i\in{1,\ldots,n}$ gegeben sind, geht der  Nearest-Neighbor-Algorithmus folgendermaßen vor:
\begin{enumerate}
    \item Es wird ein zufälliger Knoten $v_s$ als Startknoten ausgewählt.
    \item Von dem ausgewählten Knoten $v_i$ wird geprüft welche anderen Knoten des Problems $v_j \in \{1,\ldots,n\} \; j\ne i$ noch nicht besucht wurden.
    Für jeden nicht besuchten Knoten wird die Distanz zwischen $v_i$ und $v_j$ berechnet.
    Sind keine Knoten mehr verfügbar, kehrt der Handlungsreisende zum Startknoten $v_s$ zurück.
    \item Der Knoten mit der kürzesten Distanz $v_j$ wird als nächster Knoten ausgewählt.
    Die Kosten der Kante zwischen den beiden Knoten $\{v_i,v_j\}$ werden auf die Gesamtstrecke addiert.
    Von $v_j$ aus wird Schritt 2 wiederholt.\cite{BhattacharyyaDasDe2021IntelligenceEnabledResearch}
\end{enumerate}
Zur Visualisierung wollen wir die NN-Heuristik an folgendem Beispiel durchrechnen:
\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{./img/NN_Example_1}
    \caption{TSP-Beispiel mit 15 Städten zur Veranschaulichung der NN-Heuristik}
    \label{fig:nnExampleOne}
\end{figure}
Im Ersten Schritt wird eine zufällige Stadt als Startpunkt ausgewählt.
Wir wählen dazu die Stadt \textit{Hennstedt} ganz im Norden.
Von \textit{Hennstedt} wird für jede andere Stadt die Distanz anhand der Abstands-Funktion (s. Kapitel ~\ref{subsec:distanzberechnung}) berechnet.
\begin{figure}[H]
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_2}
        \caption{Auswahl Startpunkt \textit{Hennstedt}}
        \label{fig:nnExampleTwo}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_3.png}
        \caption{Berechnung Distanz zu anderen Städten}
        \label{fig:nnExampleThree}
    \end{minipage}
\end{figure}
In der folgenden Tabelle werden die Distanzen von \textit{Hennstedt} zu anderen Städten dargestellt:
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        \color{red}{Barßel} & Gehrde & Walsrode & Rheinsberg  \\
        \color{red}{155.39km} & 204.99km & 160.62km & 278.42km \\
        \hline
        Lostau & Hohendodeleben & Hadmersleben &  Herrngiersdorf \\
        287.17km & 288.59km & 292.12km & 643.04km \\
        \hline
        Landau an der Isar & Rotthalmünster & Weiler-Simmerberg & Steißlingen \\
        669.96km & 715.86km & 747.02km & 721.37km \\
        \hline
        & Nersingen & Weil im Schönbuch & \\
        & 654.65km & 629.85km & \\
        \hline
    \end{tabular}
    \caption{Distanz zu \textit{Hennstedt}}
    \label{tab:nnExampleOne}
\end{table}
Wir wählen also von \textit{Hennstedt} aus als nächste Stadt \textit{Barßel} und haben eine aktuelle Gesamtstrecke von $155.39km$.
Von \textit{Barßel} aus wiederholen wir den Vorgang und berechnen die Distanzen zu allen noch nicht besuchten Städten.
\begin{figure}[H]
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_4.png}
        \caption{Verbindung \textit{Hennstedt} $\rightarrow$ \textit{Barßel}}
        \label{fig:nnExampleFour}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_5.png}
        \caption{Berechnung Distanz zu anderen Städten}
        \label{fig:nnExampleFive}
    \end{minipage}
\end{figure}
Die nächste Stadt von \textit{Barßel} ist \textit{Gehrde} mit einer Entfernung von $68.34km$.
Der Handlungsreise besucht also im nächsten Schritt die Stadt \textit{Gehrde} und wir erhalten eine Gesamtstrecke von $223.73km$.
\begin{figure}[H]
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_6.png}
        \caption{Verbindung \textit{Hennstedt} $\rightarrow$ \textit{Barßel} $\rightarrow$ \textit{Gehrde}}
        \label{fig:nnExampleSix}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=6.5cm]{./img/NN_Example_7.png}
        \caption{Berechnung Distanz zu anderen Städten}
        \label{fig:nnExampleSeven}
    \end{minipage}
\end{figure}
Führen wir diese Schritte jetzt für jede nicht besuchte Stadt aus und kehren im Anschluss zum Startpunkt \textit{Hennstedt} zurück, erhalten wir folgende Tour mit einer Gesamtlänge von $2.479,15km$.
\\[0.2cm]
$\textit{Hennstedt} \rightarrow \textit{Barßel} \rightarrow \textit{Gehrde} \rightarrow \textit{Walsrode} \rightarrow \textit{Hohendodeleben} \rightarrow \textit{Hadmersleben} \rightarrow \textit{Lostau} \rightarrow \textit{Rheinsberg} \rightarrow \textit{Herrngiersdorf} \rightarrow \textit{Landau an der Isar} \rightarrow \textit{Rotthalmünster} \rightarrow \textit{Nersingen} \rightarrow \textit{Weil im Schönbuch} \rightarrow \textit{Steißlingen} \rightarrow \textit{Weiler-Simmerberg} \rightarrow \textit{Hennstedt}$
\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{./img/NN_Example_9.png}
    \caption{Lösung des TSP-Beispiels aus Figur~\ref{fig:nnExampleOne} mit der NN-Heuristik}
    \label{fig:nnExampleNine}
\end{figure}
