\chapter{Das Traveling Salesman Problem}\label{ch:das-traveling-salesman-problem}
Das Traveling Salesman Problem (\textit{TSP, deut. Problem des Handlungsreisenden}) ist ein mathematisches Optimierungsproblem.\cite{williams2013ModelBuildingMP}
\\[0.5cm]
\textbf{Definition 1 (Mathematisches Optimierungsproblem):}\\ Bei einem \textit{mathematischen Optimierungsproblem} wird ein reales Problem mit mathematischen Formulierungen modelliert.
Das Ziel der Optimierung ist das Finden von Extrema der Objekt-Funktion $\Phi(x)$. \cite{encylcopedia2010MP}
\\[0.5cm]
Beim Traveling Salesman Problem wird von folgender Situation ausgegangen:
Ein Händler hat den Auftrag eine Menge von Kunden zu besuchen, die sich an verschiedenen Orten befinden.  \\
Die Optimierungsaufgabe besteht jetzt darin die Reihenfolge der Kundenbesuche so zu wählen, dass die zurückgelegte Gesamtstrecke minimiert wird.\cite{williams2013ModelBuildingMP}
\\[0.5cm]
Erstmalig als mathematisches Optimierungsproblem erwähnt wurde das TSP 1930 von Karl Menger.
Seine Formulierung wird heute als Grundlage für speziellere Formen des TSP verwendet.
Das Problem des Handlungsreisenden wurde aber schon 1832 in einem Buch für Handlungsreisende thematisiert.
Der Inhalt bestand jedoch nur aus Empfehlungen für Touren in Deutschland und der Schweiz.
In dem Buch wurde kein Bezug zu mathematischen Formulierungen genommen.
In dieser Arbeit beschäftigen wir uns näher mit der ersten Formulierung als ganzzahliges lineares Programm von George Dantzig, Delbert Ray Fulkerson und Selmer M. Johnson aus dem Jahr 1954.\cite{wiki2022TravelingSalesmanProblem}
\\[0.5cm]
Über die Zeit wurden verschiedene Spezifikationen für das Traveling Salesman Problem veröffentlicht.
Die in dieser Arbeit behandelte Definition enthält folgende Spezifikationen:
\begin{enumerate}
    \item Nach dem Besuchen aller Kunden soll der Händler wieder an seinen Ausgangspunkt zurückkehren.
    Der Start- und Zielort sind damit identisch.
    \item Die Distanz zwischen zwei Städten berechnen wir aus Koordinaten und der Formel für die exakte Entfernungsberechnung für die Kugeloberfläche (s. Kapitel \ref{subsec:distanzberechnung}).
    Daher gehen wir von einem symmetrischen Traveling Salesman Problem aus, bei dem die Strecke von einer Stadt $i$ zu einer anderen Stadt $j$ gleich der Strecke von $j$ nach $i$ ist.
    \\[0.2cm]
    \hspace*{1.3cm}
    $c_{ij} = c_{ji}$
\end{enumerate}
Zum besseren Verständnis wollen wir das Problem hier visuell darstellen.
Dazu nutzen wir einen gewichteten Graph $G=(V,E)$, wobei $V$ die Städte in Form von Knoten und $E$ die Distanz zwischen zwei Städten in Form von gewichteten Kanten darstellt.
\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \node[circle,draw] (1) {$FRA$};
        \node[circle,draw, right=3cm of 1] (2) {$FRE$};
        \node[circle,draw, below=3cm of 1] (3) {$HAM$};
        \node[circle,draw, right=3cm of 3] (4) {$KIE$};
        \draw[<->,thick] (1) -- (2) node [pos=0.5,above] {242,83km};
        \draw[<->,thick] (1) -- (3) node [pos=0.5,left] {395,94km};
        \draw[<->,thick] (1) -- (4) node [pos=0.5,above, transform canvas={yshift=1cm, xshift=-5pt}] {478,50km};
        \draw[<->,thick] (2) -- (3) node [pos=0.5,below, transform canvas={yshift=-1cm, xshift=-5pt}] {638,69km};
        \draw[<->,thick] (2) -- (4) node [pos=0.5,right] {721,04km};
        \draw[<->,thick] (3) -- (4) node [pos=0.5,below] {83,32km};
    \end{tikzpicture}
    \caption{Beispiel-Graph für ein symmetrisches TSP mit vier Städten}
    \label{fig:tspExampleGraphWithDistances}
\end{figure}
Die Figur~\ref{fig:tspExampleGraphWithDistances} stellt das symmetrische Traveling Salesman Problem mit den vier Städten Frankfurt ($\texttt{FRA}$), Freiburg ($\texttt{FRE}$), Hamburg ($\texttt{HAM}$) und Kiel ($\texttt{KIE}$) dar.
Der Graph ist abstrahiert und der Abstand zwischen den Knoten stimmt nicht mit der realen Entfernung der Städte überein.
Von jeder Stadt kann der Händler in eine der anderen drei Städte gehen.\cite{williams2013ModelBuildingMP}
Dieses Beispiel wird in nachfolgenden Kapiteln verwendet, um die mathematische Definition des Traveling Salesman Problem anschaulich zu erläutern.

\section{Mathematische Formulierung des TSP}\label{sec:mathematische-formulierung-des-tsp}
Das Traveling Salesman Problem wird als lineare Ganzzahl-Optimierung modelliert.
Für die mathematische Formulierung sind folgende
 zwei Definitionen erforderlich:
\newpage
\textbf{Definition 2 (Lineare Optimierung):}\\
Die lineare Optimierung (\textit{engl. Linear Programming Model}) ist ein Ansatz zum modellieren von mathematischen Optimierungsproblemen.
Das Modell besteht aus $n$ Entscheidungsvariablen $x_1,\ldots,x_n$ und den dazugehörigen Kosten $c_1,\ldots,c_n$, wobei für die Entscheidungsvariablen gilt $x_i \ge 0 \; \forall i \in \{1,\ldots,n\}$.
Die lineare Objekt-Funktion $\Phi(x)$ wird beschrieben als
\begin{equation*}
    \sum_{i=1}^n c_i \cdot x_i.
\end{equation*}
Die Bedingung, die die Lösungsmenge einschränken, werden \textit{Constraints} genannt.
In der linearen Optimierung können Constraints nur die Form von linearen Gleichungen und Ungleichungen annehmen.\\
Die lineare Optimierung ist definiert auf
\\[0.2cm]
\hspace*{1.3cm}
$max(\{c^T \cdot x \, | \, Ax \le b, \, x \ge 0, \, x\in \mathbb{R}^n\})$,
\\[0.2cm]
wobei $c$ der Vektor der Kosten, $x$ der Vektor der Entscheidungsvariablen und die Ungleichgung $Ax\le b$ die Zusammenfassung aller Bedingungen des Modells sei.\cite{broek2018IntegerProgrammingModel}
\\[0.5cm]
\textbf{Definition 3 (Lineare Ganzzahl-Optimierung):}\\
Die lineare Ganzzahl-Optimierung (\textit{engl. Linear Integer Programming Model}) ist eine Erweiterung der $\textit{linearen Optimierung}$, bei der die Entscheidungsvariablen $x_1,\ldots, x_n$ nur ganzzahlige Werte annehmen können.\\
Die lineare Ganzzahl-Optimierung ist definiert auf
\\[0.2cm]
\hspace*{1.3cm}
$max(\{c^T \cdot x \, | \, Ax \le b, \, x \ge 0, \, x\in \mathbb{Z}^n\})$.\cite{williams2013ModelBuildingMP}
\\[0.5cm]
In unserer Arbeit wenden wir die \textit{Danzig-Fulkerson-Johnson-Formulierung} \textit{(DFJ-Formulierung)} an.
Darin wird das Traveling Salesman Problem als Entscheidungsproblem beschrieben, bei dem die Entscheidungsvariablen nur die Werte $0$ oder $1$ annehmen können.
Für die Definition unseres Problems seien $n+1$ Städte gegeben, die nach der TSP-Definition in optimaler Reihenfolge miteinander verbunden werden sollen. \\
Im Folgenden nutzen wir die Entscheidungsvariablen $\delta_{ij}$ mit $i$ der Ausgangstadt einer Strecke und $j$ der Zielstadt.
Wir modellieren die Städte numerisch.
Dann können $i$ und $j$ die Werte $0,1,2,\ldots,n$ annehmen nicht aber gleich sein.
Die Werte der Entscheidungsvariablen werden dann folgendermaßen interpretiert:
\begin{equation*}
    \label{deltaDefinitionTSPValues}
    \delta(i,j) =
    \left\{
    \begin{array}{ll}
        1, & \text{wenn eine Strecke direkt von } i \text{ nach } j \text{ verläuft} \\
        0, & \text{sonst}
    \end{array}
    \right.
\end{equation*}
Mit Hilfe der Entscheidungsvariablen können wir dann unsere Objekt-Funktion des Traveling Salesman Problems definieren.
\begin{equation}
    \sum_{i=0}^n\sum_{j=0}^{n} c_{ij} \cdot \delta_{ij} \qquad i \ne j\label{eq:objectFunction}
\end{equation}
Dabei sei $c_{ij}$ die Distanz zwischen der Stadt $i$ und der Stadt $j$.\\
Für unser Beispiel aus Figur~\ref{fig:tspExampleGraphWithDistances}  definieren wir $FRA=0$, $FRE=1$, $HAM=2$ und $KIE=3$.
Dann gilt für die Objekt-Funktion:
\begin{align*}
    &\sum_{i=0}^n\sum_{j=0, i \ne j}^{n} c_{ij} \cdot \delta_{ij} \\
    &= c_{01}\delta_{01}+c_{02}\delta_{02}+c_{03}\delta_{03}+c_{10}\delta_{10}+c_{12}\delta_{12}+c_{13}\delta_{13}\\
    &+c_{20}\delta_{20}+c_{21}\delta_{21}+c_{23}\delta_{23}+c_{30}\delta_{30}+c_{31}\delta_{31}+c_{32}\delta_{32} \\
    &=242,83\cdot \delta_{01}+395,94\cdot \delta_{02}+478,5\cdot \delta_{03}+242,83\cdot \delta_{10}+638,69\cdot \delta_{12}+721,04\cdot \delta_{13} \\
    &+395,94\cdot \delta_{20}+638,69\cdot \delta_{21}+83,32\cdot \delta_{23}+478,5\cdot \delta_{30}+721,04\cdot \delta_{31}+83,32\cdot \delta_{32}
\end{align*}
Die Variablen aus der Menge $\{\delta_{ij} \; | \; \forall i,j \in \{0,1,2,3\}: i \ne j\}$ werden dann so mit den Werten $0$ oder $1$ besetzt, dass die Summe $\sum_{i=0}^n\sum_{j=0,i \ne j}^{n} c_{ij} \cdot \delta_{ij}$ minimal ist.

\subsection{Die Degree-Constraints des TSP}\label{subsec:die-degree-constraints-des-tsp-[2]}
Die Lösungen des Traveling Salesman Problem sind als eine Tour definiert, in der jede Stadt genau einmal besucht wird.
In der Graphen-Theorie bedeutet das, dass jeder Knoten genau zwei anliegende Kanten besitzt.
Damit wird gleichzeitig festgelegt, dass die Tour ohne Unterbrechungen durchgeführt werden kann. \cite{williams2013ModelBuildingMP}\\
Daher muss eine Tour folgende Bedingungen erfüllen, um das Problem zu lösen:
\begin{enumerate}
    \item Um sicherzustellen, dass nach einer Stadt $i$ genau eine andere Stadt besucht wird, werden die Entscheidungsvariablen, die über eine Strecke zwischen Stadt $i$ und allen anderen Städten $j$ entscheidet, aufsummiert. Wenn die Summe gleich $1$ ist, wird diese Bedingung erfüllt:
    \begin{equation}
        \label{eq:tspconstraintone}
        \sum_{j=0}^{n}\delta_{ij} = 1 \qquad i=0,1,\ldots,n \; \land \; i \ne j
    \end{equation}
    \item Auf gleiche Weise wird sichergestellt, dass vor einer Stadt $j$ genau eine Stadt $i$ besucht wurde.
    Für diese Bedingung gilt:
    \begin{equation}
        \label{eq:tspconstrainttwo}
        \sum_{i=0}^{n}\delta_{ij} = 1 \qquad j=0,1,\ldots,n \; \land \; i \ne j
    \end{equation}
\end{enumerate}

\subsection{Das Subtour-Elimination-Constraint des TSP}\label{subsec:das-subtour-elimination-constraint-des-tsp-[2]}
Anhand einer dritten Bedingung soll ausgeschlossen werden, dass eine Lösung des Traveling Salesman Problem aus mehreren Subtouren besteht.
Eine Subtour ist eine in sich geschlossene Tour, die nur eine Teilmenge aller Städte besucht.
\begin{figure}[H]
    \centering
    % \includegraphics{}
    \begin{tikzpicture}
        [main/.style = {draw}, decoration={markings, mark=at position 0.5 with {\arrow{>}}}]
        \draw [-, postaction={decorate}] (-1.,2.) -- (0.,0.) node[anchor=east] {0};
        \draw [-, postaction={decorate}] (0.,0.) -- (2.,-1.) node[anchor=south] {1};
        \draw [-, postaction={decorate}] (2.,-1.) -- (3.5,0.5) node[anchor=west] {2};
        \draw [-, postaction={decorate}] (3.5,0.5) -- (3.,3.) node[anchor=south west] {3};
        \draw [-, postaction={decorate}] (3.,3.) -- (1.,3.5) node[anchor=south] {4};
        \draw [-, postaction={decorate}] (1.,3.5) -- (-1.,2.) node[anchor=south] {5};
        \draw [-, postaction={decorate}] (5.,0.) -- (6.,3.5) node[anchor=south] {6};
        \draw [-, postaction={decorate}] (6.,3.5) -- (8.,2.5) node[anchor=south west] {7};
        \draw [-, postaction={decorate}] (8.,2.5) -- (9.,-0.5) node[anchor=west] {8};
        \draw [-, postaction={decorate}] (9.,-0.5) -- (5.,0.) node[anchor=east] {9};

    \end{tikzpicture}
    \caption{Lösung für das Beispiel inkl. Subtouren}
    \label{fig:tspTourWithSubtours}
\end{figure}
Ein Beispiel für eine Lösung eines TSP mit 8 Städten wird in Figur~\ref{fig:tspTourWithSubtours} dargestellt.
Nach den beiden Constraints (\ref{eq:tspconstraintone}) und (\ref{eq:tspconstrainttwo}) würde die Tour das TSP lösen.
Um dies zu verhindern, wird in der mathematischen Modellierung das dritte Constraint
\begin{equation}
    \label{tspConstraintThree}
    \sum_{i,j\in S}\delta_{ij} \le |S|-1 \qquad \forall S \subset {1,\ldots,n} \; \land \; i \ne j
\end{equation}
hinzugefügt.
Nach dieser Gleichung muss für eine Teilmenge $S\subset V$, mit $V$ der Menge aller Städte, die Summe aller Entscheidungsvariablen, der Strecken zwischen den in $S$ enthaltenen Städten, kleiner sein, als die Anzahl der Elemente von $S$.
Wenn die Summe gleich der Anzahl der Elemente von $S$ wäre, würde das bedeuten, dass sich ein in sich geschlossener Kreis gebildet hat, der nicht alle Städte enthält, also eine Subtour.\\
Für die Anwendung des TSP bedeutet das Constraint~\ref{tspConstraintThree} die Aufstellung von exponentiell vielen möglichen Bedingungen, die Subtouren einzeln ausschließen.
TSPs mit vielen Städten sind damit nicht mehr in einer angemessenen Zeit lösbar.
Aus diesem Grund werden die Constraints dieser Form erst während der Berechnung hinzugefügt. 
Sie werden dann \textit{Lazyconstraints} genannt.
Beim Fund einer Lösung mit einer Subtour wird das zugehörige Constraint aufgestellt.
Damit werden gleichzeitig auch mehrere andere Lösungen mit Subtouren ausgeschlossen.
\\[0.5cm]
Das wollen wir kurz an dem Beispiel aus der Figur~\ref{fig:tspTourWithSubtours} erklären.
Wenn unsere Anwendung diese Lösung des TSPs findet, würde sie die kleinere Subtour betrachten und das Constraint
\begin{equation*}
    \sum_{i,j\in S}\delta_{ij} \le |S|-1 = \delta_{67}+\delta_{76}+\delta_{78}+\delta_{87}+\delta_{89}+\delta_{98}+\delta_{96}+\delta_{69} \le 3
\end{equation*}
aufstellen.
Die Bedingung ergibt sich aus der Teilmenge der Städte $S=\{6,7,8,9\}$, die in der Subtour besucht werden.
Die Summe der Distanzen zwischen allen Städten muss kleiner $|S|-1=3$ sein. \\[0.5cm]
Werden zum Lösen des Traveling Salesman Problems als mathematisches Modell die drei Bedingungen \eqref{eq:tspconstraintone}, \eqref{eq:tspconstrainttwo} und \eqref{tspConstraintThree} angewandt, werden nur korrekte Lösungen berechnet.
Aus diesen Lösungen muss dann die Tour mit der minimalen Gesamtlänge gefunden werden, um die optimale Tour zu finden.\cite{williams2013ModelBuildingMP}