import pytest
import server
import gurobi_solver

EPSILON = 0.12

@pytest.mark.parametrize("cityA, cityB, expected", [
    (server.City(name="Test1", lat=0, lng=0), server.City(name="Test2", lat=0, lng=0), 0),
    (server.City(name="Test1", lat=123, lng=123), server.City(name="Test2", lat=123, lng=123), 0),
    (server.City(name="Test1", lat=123, lng=144), server.City(name="Test2", lat=123, lng=144), 0),
    (server.City(name="Test1", lat=13, lng=14), server.City(name="Test2", lat=14, lng=13), 155.09),
    (server.City(name="Test1", lat=1, lng=54), server.City(name="Test2", lat=19, lng=43), 2333.08),
])
def test_calculate_distance(cityA, cityB, expected):
    assert abs(gurobi_solver.calculate_distance(cityA, cityB) - expected) <= EPSILON