import pytest
import server
import gurobi_solver


@pytest.mark.parametrize("cities, expected", [
    ([], []),
    ([
         server.City(name="Frankfurt", lat=50.110924, lng=8.682127),
         server.City(name="Wiesbaden", lat=50.078217, lng=8.239761),
         server.City(name="Darmstadt", lat=49.872826, lng=8.651193),
         server.City(name="Mainz", lat=49.992863, lng=8.247253)
     ], [
         server.City(name="Frankfurt", lat=50.110924, lng=8.682127),
         server.City(name="Wiesbaden", lat=50.078217, lng=8.239761),
         server.City(name="Mainz", lat=49.992863, lng=8.247253),
         server.City(name="Darmstadt", lat=49.872826, lng=8.651193)
     ]),
    ([
         server.City(name="Marburg an der Lahn", lat=50.80904, lng=8.77069),
         server.City(name="Lenzkirch", lat=47.8682042, lng=8.2022783),
         server.City(name="Eichigt", lat=50.3645048, lng=12.1751371),
         server.City(name="Joachimsthal", lat=52.9770643, lng=13.7453282)
     ], [
        server.City(name="Marburg an der Lahn", lat=50.80904, lng=8.77069),
        server.City(name="Lenzkirch", lat=47.8682042, lng=8.2022783),
        server.City(name="Eichigt", lat=50.3645048, lng=12.1751371),
        server.City(name="Joachimsthal", lat=52.9770643, lng=13.7453282)
    ])
])
def test_calculate_shortest_path(cities, expected):
    assert gurobi_solver.calculate_shortest_path(cities) == expected
