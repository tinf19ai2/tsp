import pytest
import server
import gurobi_solver

@pytest.mark.parametrize("cities, expected", [
    ([], []),
    ([server.City(name="Frankfurt", lng=0, lat=0)], ["Frankfurt"]),
    ([server.City(name="Frankfurt", lng=0, lat=0), server.City(name="Mannheim", lng=0, lat=0), server.City(name="Mainz", lng=0, lat=0)], ["Frankfurt", "Mannheim", "Mainz"]),
])
def test_get_city_names(cities, expected):
    assert gurobi_solver.get_city_names(cities) == expected
