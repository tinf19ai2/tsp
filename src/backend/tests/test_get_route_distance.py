import pytest
import server
import gurobi_solver

EPSILON = 0.05


@pytest.mark.parametrize("route, expected", [
    ([
         server.City(name="Frankfurt", lat=50.110924, lng=8.682127),
         server.City(name="Darmstadt", lat=49.872826, lng=8.651193)
     ], 0),
    ([
         server.City(name="Frankfurt", lat=50.110924, lng=8.682127),
         server.City(name="Darmstadt", lat=49.872826, lng=8.651193),
         server.City(name="Mainz", lat=49.992863, lng=8.247253),
         server.City(name="Wiesbaden", lat=50.078217, lng=8.239761),
     ], 99.68214329230297),
])
def test_(route, expected):
    assert abs(gurobi_solver.get_route_distance(route) - expected) <= EPSILON
