from queue import Queue
from datetime import datetime
import queue
import json

import server

from sqlalchemy import select
from sqlalchemy.orm import Session

from itertools import combinations
import gurobipy as gp

from math import radians, sin, cos, acos

q = Queue()

def tsp_worker():
    global q
    session = server.get_session()
    while True:
        submission = q.get()
        stmt = select(server.Submission).where(server.Submission.id == submission["submission_id"])
        result = session.scalars(stmt).one()
        result.status = "STARTED"
        session.commit()
        complete_distance = get_route_distance(submission["path"])
        shortest_path = calculate_shortest_path(submission["path"])
        shortest_distance = get_route_distance(shortest_path)

        result = session.scalars(stmt).one()
        result.status = "DONE"
        result.finished = datetime.now()
        result.solution = str.encode(json.dumps(shortest_path, default=vars))
        result.complete_distance = complete_distance
        result.calculated_distance = shortest_distance

        session.commit()

        q.task_done()


def add_to_queue(path, id):
    global q

    q.put({"submission_id": id, "path": path})


def calculate_distance(start_city, end_city):
    if start_city.lat == end_city.lat and start_city.lng == end_city.lng:
        return 0
    return 6371 * acos(
        sin(radians(start_city.lat)) * sin(radians(end_city.lat)) + 
        cos(radians(start_city.lat)) * cos(radians(end_city.lat)) * cos(radians(start_city.lng - end_city.lng))
    )


def get_city_names(cities):
    return [city.name for city in cities]


def calculate_shortest_path(cities):
    m = gp.Model()
    city_names = get_city_names(cities)

    # create dictionary with calculated distances between each pair of cities
    dist = {(start_city.name, end_city.name): calculate_distance(start_city, end_city)
            for start_city, end_city in combinations(cities, 2)}

    # add dictionary to model -> Gurobi-Object
    vars = m.addVars(dist.keys(), obj=dist, vtype=gp.GRB.BINARY, name='x')

    # forward and backward are the same -> transform directional tsp to undirectional tsp
    for i, j in vars.keys():
        vars[j, i] = vars[i, j]

    # 1.Constraint: Each city is connected to exact two other cities. (Zuweisung?)
    m.addConstrs(vars.sum(c, '*') == 2 for c in city_names)

    def eliminate_subtour(model, where):
        if where == gp.GRB.Callback.MIPSOL:  # necessary for lazy constraint
            vals = model.cbGetSolution(model._vars)  # indicates whether i and j are connected

            # select all tuples of two cities which are connected with a probability greater then 0.5
            selected = gp.tuplelist((i, j) for i, j in model._vars.keys() if vals[i, j] > 0.5)
            tour = subtour(selected, city_names)
            if len(tour) < len(city_names):
                # add lazy constraint to every pair of cities in subtour which eliminates this subtour
                model.cbLazy(gp.quicksum(model._vars[i, j] for i, j in combinations(tour, 2)) <= len(tour) - 1)

    m._vars = vars
    m.Params.LazyConstraints = 1
    m.optimize(eliminate_subtour)

    vals = m.getAttr('x', vars)
    selected = gp.tuplelist((i, j) for i, j in vals.keys() if vals[i, j] > 0.5)
    cities.sort(key=lambda x: subtour(selected, city_names).index(x.name))
    return cities


def subtour(edges, city_names):
    unvisited = city_names[:]  # slice to copy
    cycle = city_names[:]  # slice to copy
    while unvisited:
        this_cycle = []
        neighbors = unvisited
        while neighbors:
            current = neighbors[0]
            this_cycle.append(current)
            unvisited.remove(current)

            # create new list of single values with all cities connected to current which are in unvisited
            neighbors = [j for i, j in edges.select(current, '*') if j in unvisited]
        if len(this_cycle) <= len(cycle):
            cycle = this_cycle  # new shortest subtour
    return cycle


def get_route_distance(route):
    complete_distance = 0
    if len(route) > 2:
        for index in range(len(route) - 1):
            complete_distance += calculate_distance(route[index], route[index + 1])
        complete_distance += calculate_distance(route[0], route[-1])
    return complete_distance