import json
import random
import os
import string

from typing import List, Optional
from datetime import datetime
from threading import Thread


from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from pydantic import BaseModel

from sqlalchemy import Column, create_engine, String, BLOB, DateTime, Float, select
from sqlalchemy.orm import declarative_base, Session


Base = declarative_base()
THREAD_COUNT = 3
JSON_DIR = "cities_per_country/"

class Submission(Base):
    __tablename__ = "submission"

    id = Column(String(32), primary_key=True)
    status = Column(String(20))
    started = Column(DateTime)
    finished = Column(DateTime)
    solution = Column(BLOB)
    complete_distance = Column(Float)
    calculated_distance = Column(Float)


class City(BaseModel):
    name: str
    lat: float
    lng: float

    class Config:
        schema_extra = {
            "example": {
                "name": "Frankfurt",
                "lat": 50.110924,
                "lng": 8.682127
            }
        }


app = FastAPI()
engine = create_engine("sqlite:///test.db", echo=True)
session = Session(engine)
Base.metadata.create_all(engine)

def get_session():
    return Session(engine)


import gurobi_solver

for i in range(THREAD_COUNT):
    worker = Thread(target=gurobi_solver.tsp_worker)
    worker.daemon = True
    worker.start()

origins = ["http://localhost:8080"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"]
)


def read_all_country_json():
    countries = dict()
    json_files = [file for file in os.listdir(JSON_DIR) if file.endswith(".json")]
    for file in json_files:
        with open(JSON_DIR + file, 'r', encoding='utf-8') as data:
            countries[file.split(".")[0]] = json.loads(data.read())
    return countries


cities = read_all_country_json()



def generate_random_id(len):
    return ''.join(random.SystemRandom().choice(string.digits + string.ascii_letters) for _ in range(len))


@app.get("/maps/")
async def get_maps():
    return [city for city in cities.keys() if os.path.isfile(f"./maps/{city}.png")]

@app.get("/maps/{map_name}/", response_class=FileResponse)
async def get_map(map_name):
    if map_name not in cities:
        raise HTTPException(status_code=404, detail="City not found") 
    return FileResponse(f"./maps/{map_name}.png")


@app.get("/cities/{map_name}/")
async def read_cities(map_name: str, city_count: int):
    if map_name not in cities:
        raise HTTPException(status_code=404, detail="City not found") 
    local_cities = cities[map_name]["cities"][:]
    for i in range(0, (len(local_cities) - city_count)):
        random_item_from_cities_list = random.choice(local_cities)
        local_cities.remove(random_item_from_cities_list)
    return {"cities": local_cities, "minLng": cities[map_name]["minLng"], "maxLng": cities[map_name]["maxLng"], "minLat": cities[map_name]["minLat"], "maxLat": cities[map_name]["maxLat"]}


@app.post("/calculateRoute")
async def calculate_route(submit_cities: List[City]):
    id = generate_random_id(32)
    session.add(Submission(id=id, started=datetime.now(), status="STARTED"))
    session.commit()
    gurobi_solver.add_to_queue(submit_cities, id)
    return {"status": "STARTED", "id": id}

@app.get("/test")
async def test():
    id = generate_random_id(32)
    session.add(Submission(id=id, started=datetime.now(), status="STARTED"))
    return {"status": "STARTED", "id": id}



@app.get("/submissions/{submission_id}")
async def get_result_from_submission(submission_id):
    stmt = select(Submission).where(Submission.id == submission_id)

    result = session.execute(stmt).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Submission not found") 
    if result[0].status == "DONE":
        return {
            "status": result[0].status,
            "routeDistance": result[0].complete_distance,
            "shortestPath": json.loads(result[0].solution),
            "shortestDistance": result[0].calculated_distance,
            "isShortestPath": result[0].complete_distance == result[0].calculated_distance
        }
    return {
        "status": result[0].status
    }