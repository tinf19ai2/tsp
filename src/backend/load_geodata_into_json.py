import json
import random

import requests
from opencage.geocoder import OpenCageGeocode, InvalidInputError, RateLimitExceededError, UnknownError

countries = ['germany', 'france', 'spain', 'italy', 'austria']
counties_to_acronym = {'germany': 'DE', 'france': 'FR', 'spain': 'ES', 'italy': 'IT', 'austria': 'AT'}
csc_api_key = 'YOUR_API_KEY_HERE'
open_cage_api_key = 'YOUR_API_KEY_HERE'
# open_cage_api_key = 'YOUR_API_KEY_HERE'


def get_cities_of_country(country):
    api_url = f"https://api.countrystatecity.in/v1/countries/{counties_to_acronym[country]}/cities"
    headers = {'X-CSCAPI-KEY': csc_api_key}
    response = requests.request("GET", api_url, headers=headers)
    if response.status_code != 200:
        print("There was an error in cities api: " + response.text)
        return []
    all_cities = [f['name'] for f in eval(response.text)]
    local_cities = all_cities.copy()
    selected_cities = []
    for i in range(1000):
        if len(local_cities) < 1:
            continue
        random_city = random.choice(local_cities)
        selected_cities.append(random_city)
        local_cities.remove(random_city)
    return selected_cities


def get_geocode_from_cities(cities, country):
    cities_data = []
    queries = [(f'{city}, {country}', city) for city in cities]
    with OpenCageGeocode(open_cage_api_key) as geocoder:
        try:
            results = [(geocoder.geocode(query[0]), query[1]) for query in queries]
            for result in results:
                cities_data.append({
                    'name': result[1],
                    'lat': result[0][0]['geometry']['lat'],
                    'lng': result[0][0]['geometry']['lng']
                })
        except InvalidInputError as ex:
            print(ex)
        except RateLimitExceededError as ex:
            print(ex)
        except UnknownError as ex:
            print(ex)
    return cities_data


for country in countries:
    json_object = json.dumps(get_geocode_from_cities(get_cities_of_country(countries[i]), countries[i]), indent=2)
    with open(f"cities_per_country/{countries[i]}.json", "w", encoding='utf-8') as outfile:
        outfile.write(json_object)