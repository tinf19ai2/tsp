# TSP

## Requirements
- Python 
- Node and npm

## Setup Backend
 1. Create a new python virtual env in the backend folder with: `python -m venv tsp`
 2. Activate the venv with the coresponding Script of your os: `.\tsp\Scripts\activate.ps1`
 3. Install the requirements from the requirement.txt with pip: `pip install -r requirement.txt`
 4. Start the server with uvicorn: `uvicorn server:app --reload` the `--reload` flag is for hot reloading

## Setup Frontend
1. Install all packages in the frontend folder with: `npm install`
2. Start the development mode with npm: `npm run serve`
3. Open http://localhost:8080 in your browser



### New python package 
1. Install 
2. `pip freeze > .\requirement.txt`